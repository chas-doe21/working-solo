# Working solo

- [Working solo](#working-solo)
- [Define "working solo"](#define-working-solo)
- [Pros & Cons](#pros--cons)
- [The solo workflow](#the-solo-workflow)
- [Agile workflow](#agile-workflow)
  - [SCRUM or Kanban?](#scrum-or-kanban)
  - [SCRUM](#scrum)
  - [Kanban](#kanban)
  - [Agile tips](#agile-tips)
- [Time management](#time-management)
  - [Track your time](#track-your-time)
  - [Don't forget admin time](#dont-forget-admin-time)
  - [Time Estimation](#time-estimation)
  - [Time estimation loop](#time-estimation-loop)
  - [How to estimate tasks](#how-to-estimate-tasks)
    - [Relative estimation](#relative-estimation)
    - [Velocity](#velocity)
    - [Evidence Based Scheduling](#evidence-based-scheduling)
    - [EBS - how it works](#ebs---how-it-works)
  - [Automate away](#automate-away)
- [Planning](#planning)
  - [Minimal Viable Product (MVP)](#minimal-viable-product-mvp)
  - [Retrospective](#retrospective)
  - [Bugs first, features second](#bugs-first-features-second)
  - [Limit your WIP](#limit-your-wip)
  - [YAGNI](#yagni)
- [The most important part of it all](#the-most-important-part-of-it-all)

# Define "working solo"

Working solo can mean different things, here are some examples:

-   you are the only person in your team/dept (but still have to show up to office, have to sync other depts, etc)
-   you are the only person on the team assigned/accustomed to a certain project (not exactly solo but bear with me)
-   you are self-employed and the only person in your company, working as a consultant
-   You are working alone on a side-project in your free time

These are just some examples.

Each scenario has its own particular set of pros/cons, challenges and opportunities.

I'll try to keep this presentation generic enough to be relevant for whichever scenario you're in.


# Pros & Cons

| PROs                                   | CONs                                                 |
|-------------------------------------- |---------------------------------------------------- |
| Flexibility                            | No colleagues to ask help to                         |
| Less bureaucracy                       | Less structure (or up to you to structure things up) |
| Potentially a better work/life balance | Potentially a worse work/life balance                |
| Better flow, less interruptions        | Can get lonely                                       |
| Less office politics                   | Possibly direct contact with customers               |


# The solo workflow

Working solo as a developer / DevOps can present many challenges. We can categorize three main areas that are very relevant here:

1.  Agile workflow
2.  Time management
3.  Planning


# Agile workflow


## SCRUM or Kanban?

It's ultimately up to you what you'll choose to work with, SCRUM or Kanban; it may depend on the nature of the project(s), what works best for you as an individual and your experience with either method.

Do make a choice, don't just use whatever without a plan.


## SCRUM

If you go the scrum way, keep in mind to:

-   keep your sprints short
-   keep your user stories small
-   do sprint and product burndown


## Kanban

If you use Kanban make sure it's an explicit choice you've made and planned.

There's nothing wrong with "using Kanban", just don't "end up using Kanban".


## Agile tips

Irrelevant of which method you choose to adopt, it can be good to:

-   keep a product backlog to have an idea of where you're going
-   contact customer/boss/stakeholder/product owner/etc often
-   use TDD/BDD


# Time management

Working solo means all the hours needed for the project will come from your workday. It's important to manage and plan your time well to avoid unhealthy work/life balance, and keep productivity up (you're not going to be productive if you're burnout).


## Track your time

Use whichever tool you like (there are a plethora), but do keep track of your work time.

In certain cases it may be a requirement from a customer; but even if it's not, tracking your time will help you better organize.


## Don't forget admin time

When planning your time, and planning a project, don't forget there is always some time that goes towards preparation and administration. This is time during which you're not directly "productive".

Example of "administration and preparation time":

-   prepare an offer for a customer
-   prepare the invoice to send the customer
-   read up on a few topics/libraries to be able to plan the project further
-   plan the project/sprint
-   review the project/sprint
-   review your tracked time
-   &#x2026;

If you're self employed there will be a whole new category of admin-time, such as: bookkeeping, bureaucracy (taxes, VAT, salaries, &#x2026;), buying the hardware you need, pay the bills for your subscriptions/cloud provider/office rent, get treated like dirt by your local bank, find and keep customers,&#x2026;


## Time Estimation

To be able to plan your time you'll need not only to keep track of it, but also to be able to estimate how long different task will take you.

It will never be a 100% spot-on estimate. Estimate too low and you'll find yourself working late at night or missing deadlines; estimate too high and you'll get big gaps between projects that may not be billable. Another risk of estimating too high is scaring away customers by leaving offers that are too expensive (if you get paid by the hour).

Time estimation is an art in and of itself. The best way of getting better at it is to practice and review.


## Time estimation loop

1.  Estimate a task
2.  Measure/track the time it took
3.  Review and reflect
4.  Repeat from step 1.


## How to estimate tasks

Here's a few tips for estimating time


### Relative estimation

When estimating several tasks, use their relative difference in how time consuming they are to help you plan.

EXAMPLE:

Task A is at least twice as time-consuming as task B, which in turn is slightly more time-consuming than task C.

My estimation could be something like:

-   TASK A: 6 hrs
-   TASK B: 3 hrs
-   TASK C: 2 hrs

This will help you keep your estimations relatively scaled from one task to the next.

If your estimate is off, it will be off in a proportional way for all tasks.


### Velocity

If you use "points" and not a time measure to quantify your tasks, get to know yourself and your work velocity: how many points do you usually get done in a day? And in a week?


### Evidence Based Scheduling

[Evidence Based Scheduling (EBS)](https://www.joelonsoftware.com/2007/10/26/evidence-based-scheduling/) is a technique for time estimating that can help you get better at estimating your time without the need to actually get better at estimating your time.

It stands on two very important ideas:

-   estimating time and scheduling is a pain in the ass
-   people suck at estimating time

It allows you to get better estimates without even learning to avoid bias during estimation, it instead uses our bias as a starting point.


### EBS - how it works

It's all very well explained in [this article](https://www.joelonsoftware.com/2007/10/26/evidence-based-scheduling/), but the gist of it is:

1.  Collect historical data about tasks: how much time was estimated and how much time it actually took
2.  compile a history of the user's velocities (actual time / estimate)
3.  estimate your new tasks
4.  do a simulation where each task's estimate is multiplied by an historical velocity
5.  do step 4 100 times, consider each iteration as having a 1% probability of being true. What's your new estimate?


## Automate away

Automate away as much as you can. It will often fall under "unbillable admin time", but in the long run you'll have more productive time on your hands.


# Planning

So now we've chosen an agile method, learned how to track our time and estimate incoming tasks.

How do we go further and plan our projects?


## Minimal Viable Product (MVP)

An MVP is about reaching a usable state as early as possible during development, and working from there.

We'll have at all times a working product (even if not 100% complete).

This is usually a good strategy when starting a new product, or working on a side-project. It is less viable when inheriting a big and old codebase from someone.


## Retrospective

Reflect and analyze your previous projects. Make a habit of reviewing your past projects, sprints and user stories.

This will usually fall under "unbillable admin time", but it is one of the best ways you have to learn from your past.


## Bugs first, features second

It can be a good strategy to give priority to fixing bugs instead of developing new features.

This may not always be a viable strategy. Do involve your customer/boss/teamlead/product owner in the prioritization process!


## Limit your WIP

Try to limit both the amount of projects you have ongoing simultaneously; as well as the number of user stories in your sprints.

Worse come to worse, if you've bitten off more than you can chew you have no-one to help you that can take over some tasks.


## YAGNI

When planning and defining which features you want to develop (assuming this hasn't been decided by the customer/product owner), remember YAGNI:

[You Aren't Gonna Need It](https://en.wikipedia.org/wiki/You_aren't_gonna_need_it)

Also, don't forget to KISS:

[Keep It Simple, Stupid](https://en.wikipedia.org/wiki/KISS_principle)


# The most important part of it all

In the end working solo is all about what works for YOU.

It won't be perfect from the beginning, but you'll eventually find your way of doing things.

Working solo isn't for everyone, but knowing how to organize and plan your work and time are valuable skills to have even when part of a big team.
% Created 2022-04-08 Fri 17:49
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\date{}
\title{Working Solo}
\hypersetup{
 pdfauthor={},
 pdftitle={Working Solo},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.5.2)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Define "working solo"}
\label{sec:org16bf183}
Working solo can mean different things, here are some examples:

\begin{itemize}
\item you are the only person in your team/dept (but still have to show up to
office, have to sync other depts, etc)
\item you are the only person on the team assigned/accustomed to a certain project
(not exactly solo but bear with me)
\item you are self-employed and the only person in your company, working as a
consultant
\item You are working alone on a side-project in your free time
\end{itemize}

These are just some examples.

Each scenario has its own particular set of pros/cons, challenges and
opportunities.

I'll try to keep this presentation generic enough to be relevant for whichever
scenario you're in.
\section*{Pros \& Cons}
\label{sec:org3fe1f5a}
\begin{center}
\begin{tabular}{ll}
PROs & CONs\\
\hline
Flexibility & No colleagues to ask help to\\
Less bureaucracy & Less structure (or up to you to structure things up)\\
Potentially a better work/life balance & Potentially a worse work/life balance\\
Better flow, less interruptions & Can get lonely\\
Less office politics & Possibly direct contact with customers\\
\end{tabular}
\end{center}
\section*{The solo workflow}
\label{sec:org3f03cd0}
Working solo as a developer / DevOps can present many challenges. We can
categorize three main areas that are very relevant here:

\begin{enumerate}
\item Agile workflow
\item Time management
\item Planning
\end{enumerate}
\section*{Agile workflow}
\label{sec:org4fbf4e3}
\subsection*{SCRUM or Kanban?}
\label{sec:org828bc91}
It's ultimately up to you what you'll choose to work with, SCRUM or Kanban; it
may depend on the nature of the project(s), what works best for you as an
individual and your experience with either method.

Do make a choice, don't just use whatever without a plan.
\subsection*{SCRUM}
\label{sec:org7e1dce3}
If you go the scrum way, keep in mind to:

\begin{itemize}
\item keep your sprints short
\item keep your user stories small
\item do sprint and product burndown
\end{itemize}
\subsection*{Kanban}
\label{sec:org7d73c1e}
If you use Kanban make sure it's an explicit choice you've made and planned.

There's nothing wrong with "using Kanban", just don't "end up using Kanban".
\subsection*{Agile tips}
\label{sec:orgc6fa87c}
Irrelevant of which method you choose to adopt, it can be good to:

\begin{itemize}
\item keep a product backlog to have an idea of where you're going
\item contact customer/boss/stakeholder/product owner/etc often
\item use TDD/BDD
\end{itemize}
\section*{Time management}
\label{sec:org2380460}
Working solo means all the hours needed for the project will come from your
workday. It's important to manage and plan your time well to avoid unhealthy
work/life balance, and keep productivity up (you're not going to be productive
if you're burnout).
\subsection*{Track your time}
\label{sec:org40fd9b5}
Use whichever tool you like (there are a plethora), but do keep track of your
work time.

In certain cases it may be a requirement from a customer; but even if it's not,
tracking your time will help you better organize.
\subsection*{Don't forget admin time}
\label{sec:orga929178}
When planning your time, and planning a project, don't forget there is always
some time that goes towards preparation and administration. This is time during
which you're not directly "productive".

Example of "administration and preparation time":

\begin{itemize}
\item prepare an offer for a customer
\item prepare the invoice to send the customer
\item read up on a few topics/libraries to be able to plan the project further
\item plan the project/sprint
\item review the project/sprint
\item review your tracked time
\item \ldots{}
\end{itemize}

If you're self employed there will be a whole new category of admin-time, such
as: bookkeeping, bureaucracy (taxes, VAT, salaries, \ldots{}), buying the hardware
you need, pay the bills for your subscriptions/cloud provider/office rent, get
treated like dirt by your local bank, find and keep customers,\ldots{}
\subsection*{Time Estimation}
\label{sec:orgab46147}
To be able to plan your time you'll need not only to keep track of it, but also
to be able to estimate how long different task will take you.

It will never be a 100\% spot-on estimate. Estimate too low and you'll find
yourself working late at night or missing deadlines; estimate too high and
you'll get big gaps between projects that may not be billable. Another risk of
estimating too high is scaring away customers by leaving offers that are too
expensive (if you get paid by the hour).

Time estimation is an art in and of itself. The best way of getting better at it
is to practice and review.
\subsection*{Time estimation loop}
\label{sec:orgd2c2083}
\begin{enumerate}
\item Estimate a task
\item Measure/track the time it took
\item Review and reflect
\item Repeat from step 1.
\end{enumerate}
\subsection*{How to estimate tasks}
\label{sec:orgf52670b}
Here's a few tips for estimating time
\subsubsection*{Relative estimation}
\label{sec:org4103fd8}
When estimating several tasks, use their relative difference in how time
consuming they are to help you plan.

EXAMPLE:

Task A is at least twice as time-consuming as task B, which in turn is slightly
more time-consuming than task C.

My estimation could be something like:
\begin{itemize}
\item TASK A: 6 hrs
\item TASK B: 3 hrs
\item TASK C: 2 hrs
\end{itemize}

This will help you keep your estimations relatively scaled from one task to the
next.

If your estimate is off, it will be off in a proportional way for all tasks.
\subsubsection*{Velocity}
\label{sec:org96ca8e1}
If you use "points" and not a time measure to quantify your tasks, get to know
yourself and your work velocity: how many points do you usually get done in a
day? And in a week?
\subsubsection*{Evidence Based Scheduling}
\label{sec:orgcbe09a7}
\href{https://www.joelonsoftware.com/2007/10/26/evidence-based-scheduling/}{Evidence Based Scheduling (EBS)} is a technique for time estimating that can help
you get better at estimating your time without the need to actually get better
at estimating your time.

It stands on two very important ideas:

\begin{itemize}
\item estimating time and scheduling is a pain in the ass
\item people suck at estimating time
\end{itemize}

It allows you to get better estimates without even learning to avoid bias during
estimation, it instead uses our bias as a starting point.
\subsubsection*{EBS - how it works}
\label{sec:org9461c3e}
It's all very well explained in \href{https://www.joelonsoftware.com/2007/10/26/evidence-based-scheduling/}{this article}, but the gist of it is:

\begin{enumerate}
\item Collect historical data about tasks: how much time was estimated and how much
time it actually took
\item compile a history of the user's velocities (actual time / estimate)
\item estimate your new tasks
\item do a simulation where each task's estimate is multiplied by an historical
velocity
\item do step 4 100 times, consider each iteration as having a 1\% probability of
being true. What's your new estimate?
\end{enumerate}
\subsection*{Automate away}
\label{sec:org9abfc4e}
Automate away as much as you can. It will often fall under "unbillable admin
time", but in the long run you'll have more productive time on your hands.
\section*{Planning}
\label{sec:org1225e4a}
So now we've chosen an agile method, learned how to track our time and estimate
incoming tasks.

How do we go further and plan our projects?
\subsection*{Minimal Viable Product (MVP)}
\label{sec:org53d47da}
An MVP is about reaching a usable state as early as possible during development,
and working from there.

We'll have at all times a working product (even if not 100\% complete).

This is usually a good strategy when starting a new product, or working on a
side-project. It is less viable when inheriting a big and old codebase from
someone.
\subsection*{Retrospective}
\label{sec:org6c3f738}
Reflect and analyze your previous projects. Make a habit of reviewing your past
projects, sprints and user stories.

This will usually fall under "unbillable admin time", but it is one of the best
ways you have to learn from your past.
\subsection*{Bugs first, features second}
\label{sec:org23a01ff}
It can be a good strategy to give priority to fixing bugs instead of developing
new features.

This may not always be a viable strategy. Do involve your
customer/boss/teamlead/product owner in the prioritization process!
\subsection*{Limit your WIP}
\label{sec:org867e452}
Try to limit both the amount of projects you have ongoing simultaneously; as
well as the number of user stories in your sprints.

Worse come to worse, if you've bitten off more than you can chew you have no-one
to help you that can take over some tasks.
\subsection*{YAGNI}
\label{sec:orgbe732f2}
When planning and defining which features you want to develop (assuming this
hasn't been decided by the customer/product owner), remember YAGNI:

\href{https://en.wikipedia.org/wiki/You\_aren't\_gonna\_need\_it}{You Aren't Gonna Need It}

Also, don't forget to KISS:

\href{https://en.wikipedia.org/wiki/KISS\_principle}{Keep It Simple, Stupid}
\section*{The most important part of it all}
\label{sec:org0f57f45}
In the end working solo is all about what works for YOU.

It won't be perfect from the beginning, but you'll eventually find your way of
doing things.

Working solo isn't for everyone, but knowing how to organize and plan your work
and time are valuable skills to have even when part of a big team.
\end{document}